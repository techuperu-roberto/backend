package com.techU.hachathon.equipo6.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;

import java.io.Serializable;
import java.util.List;

@Document(collection = "questions")
@JsonPropertyOrder({"questionsID","detail","answerOptions","correctAnswer"})
public class Questions implements Serializable {
    @Id
    @NonNull
    private Integer questionsID;
    private String detail;
        private List<arregloOpciones> answerOptions;
    private Integer correctAnswer;

    public Questions(@NonNull Integer questionsID,String detail, List<arregloOpciones> answerOptions, Integer correctAnswer) {
        this.setQuestionsID(questionsID);
        this.setDetail(detail);
        this.setAnswerOptions(answerOptions);
        this.setCorrectAnswer(correctAnswer);
    }

    @NonNull
    public Integer getQuestionsID() {
        return questionsID;
    }

    public void setQuestionsID(@NonNull Integer questionsID) {
        this.questionsID = questionsID;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public List<arregloOpciones> getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(List<arregloOpciones> answerOptions) {
        this.answerOptions = answerOptions;
    }

    public Integer getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(Integer correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
