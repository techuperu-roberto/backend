package com.techU.hachathon.equipo6.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "answers")
@JsonPropertyOrder({"id", "email", "answers"})
public class answers {

    @Id
    @NotNull
    private String id;
    private String email;
    @NotNull
    private List<listAnswers> answers;


    //   @NotNull
 //   private Integer questionId;
 //   @NotNull
 //   private Integer answerId;


    public answers(@NotNull String id, String email, List<listAnswers> answers) {
        this.id = id;
        this.email = email;
        this.answers = answers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<listAnswers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<listAnswers> answers) {
        this.answers = answers;
    }
}
