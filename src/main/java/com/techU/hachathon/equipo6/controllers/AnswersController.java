package com.techU.hachathon.equipo6.controllers;

import com.techU.hachathon.equipo6.models.answers;
import com.techU.hachathon.equipo6.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping ("answers")
public class AnswersController {
    private final AnswerService answerService;
    private answers answers;

    @Autowired
    public AnswersController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping
    public ResponseEntity<List<answers>> answers(){
        System.out.println("Me piden la lista de respuestas");
        return ResponseEntity.ok(answerService.findAll());
    }

    @PostMapping
    public ResponseEntity<Map> saveAnswers(@RequestBody answers answers){
        System.out.println("Me piden crear las respuestas");
  //      System.out.println(answers.getAnswers().get(0).getAnswerID());
  //      System.out.println(answers.getAnswers().get(0).getQuestionID());
        answers answersCorrect= (answerService.saveAnswers(answers));
        Map respuesta = new HashMap();
        respuesta.put("dictamen","APROBADO");
        respuesta.put("rightQuestions","3");
        respuesta.put("wrongQuestions","2");
        respuesta.put("puntaje", "10");
        respuesta.put("examenID",answersCorrect.getId());
        return ResponseEntity.ok(respuesta);

    }
}
