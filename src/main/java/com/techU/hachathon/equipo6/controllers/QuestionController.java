package com.techU.hachathon.equipo6.controllers;

import com.techU.hachathon.equipo6.models.Questions;
import com.techU.hachathon.equipo6.service.QuestionSerevice;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("questions")
public class QuestionController {
    private final QuestionSerevice questionSerevice;
    private Questions questions;

    @Autowired
    public QuestionController(QuestionSerevice questionSerevice){
        this.questionSerevice = questionSerevice;
    }

    @GetMapping()
    public ResponseEntity<List<Questions>> getQuestionsFromMongo(){
        System.out.println("Me piden la lista de preguntas");
        return ResponseEntity.ok(questionSerevice.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Questions> getQuestionFromMongo(@PathVariable("id") Integer id){
        System.out.println("me preguntan por una pregunta");
        System.out.println(questionSerevice.findOne(id));
        return ResponseEntity.ok(questionSerevice.findOne(id));
    }
    //trae todas las pregunutas cargadas en el bucket de AWS
    @GetMapping("/v1")
    public ResponseEntity<JSONArray> getOneQuestionsFromAWS(){
        System.out.println("me preguntan por la lista de preguntas desde AWS");
        return ResponseEntity.ok(questionSerevice.findAllinAWS());
    }

    //devuelve una pregunta del total de preguntas disponibles
    @GetMapping("/v1/{id}")
    public ResponseEntity<String> getOneQuestionsFromAWS(@PathVariable("id") Integer id) {
        System.out.println("me preguntan por una pregunta de la lista desde AWS");
        return ResponseEntity.ok(questionSerevice.findoneinAWS(id));
    }

    @GetMapping("/v1/random/{quantity}")
    public ResponseEntity<JSONArray> getRandomQuestions(@PathVariable("quantity") Integer quantity) {
        System.out.println("me preguntan por la lista aleatoria de preguntas");
        return ResponseEntity.ok(questionSerevice.findRandomQuestions(quantity));
    }
}
