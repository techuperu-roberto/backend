package com.techU.hachathon.equipo6.service;

import com.google.gson.JsonObject;
import com.techU.hachathon.equipo6.models.Questions;
import org.json.simple.JSONArray;

import java.util.List;

public interface QuestionSerevice{
    List<Questions> findAll();
    public Questions findOne(Integer ID);
    public JSONArray findAllinAWS();
    public String findoneinAWS(Integer ID);
    public JSONArray findRandomQuestions(Integer quantity);

}
