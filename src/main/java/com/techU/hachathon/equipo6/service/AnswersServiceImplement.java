package com.techU.hachathon.equipo6.service;

import com.techU.hachathon.equipo6.Repository.AnswerRepository;
import com.techU.hachathon.equipo6.models.answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("AnswerService")
@Transactional
public class AnswersServiceImplement implements AnswerService{
    private AnswerRepository answerRepository;

    @Autowired
    public AnswersServiceImplement (AnswerRepository answerRepository) {
        this.answerRepository =answerRepository;
    }

    @Override
    public List<answers> findAll() {
        return answerRepository.findAll();
    }

    @Override
    public answers findOne(String id) {
        return answerRepository.findOne(id);
    }

    @Override
    public answers saveAnswers(answers ans) {
        return answerRepository.saveAnswers(ans);
    }

    @Override
    public void updateAnswers(answers ans) {
        answerRepository.updateAnswers(ans);

    }

    @Override
    public void deleteAnswers(String id) {
    }
}
