package com.techU.hachathon.equipo6.service;

import com.google.gson.Gson;
import com.techU.hachathon.equipo6.Repository.QuestionsRepository;
import com.techU.hachathon.equipo6.models.Questions;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

@Service("questionService")
@Transactional
public class QuestionServiceImpl implements QuestionSerevice {
    private QuestionsRepository questionsRepository;

    @Autowired
    public QuestionServiceImpl(QuestionsRepository questionsRepository) {
        this.questionsRepository = questionsRepository;
    }

    @Override
    public List<Questions> findAll() {
        return questionsRepository.findAll();

    }

    @Override
    public Questions findOne(Integer id) {
        return questionsRepository.findOne(id);
    }

    @Override
    public JSONArray findAllinAWS() {
        String urlAPI = "https://questionsucamec.s3-sa-east-1.amazonaws.com/questions.json";
        JSONArray questions;
        JSONParser parser = new JSONParser();

        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("User-Agente", "Myapp");
            conexion.setRequestProperty("Content.Type", "application/json; charset=utf-8");
            InputStreamReader entrada = new InputStreamReader(conexion.getInputStream());
            int respuesta = conexion.getResponseCode();
            System.out.println(new StringBuilder().append("Respuesta Servicio: ").append(respuesta));
            if (respuesta == HttpURLConnection.HTTP_OK) {
                BufferedReader lector = new BufferedReader(entrada);
                String lineaLeida;
                StringBuffer resultado = new StringBuffer();
                while ((lineaLeida = lector.readLine()) != null) {
                    resultado.append(lineaLeida);
                }
                lector.close();
                Gson json = new Gson();
                questions = json.fromJson(String.valueOf(resultado), JSONArray.class);
                return (questions);
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String findoneinAWS(Integer ID){
        Gson gson = new Gson();
        String json = gson.toJson(findAllinAWS().get(ID-1));
        return json;
    }

    @Override
    public JSONArray findRandomQuestions(Integer quantity) {
        JSONArray arreglo = findAllinAWS();
        if (quantity == 5) {
            JSONArray questions = new JSONArray();
            for (int x = 0; x <quantity; x++){
                int j = (int) Math.floor(Math.random() * (arreglo.size()-1));
                questions.add(arreglo.get(j));
                arreglo.remove(j);
            }
            return arreglo;
        }
        return null;
    }
}