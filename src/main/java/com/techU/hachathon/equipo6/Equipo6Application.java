package com.techU.hachathon.equipo6;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Equipo6Application {

	public static void main(String[] args) {
		SpringApplication.run(Equipo6Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunnerLog(ApplicationContext ctx)
	{
		return args -> {
			System.out.println("Spring Boot funcionando");
		};
	}

}
