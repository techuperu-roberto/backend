package com.techU.hachathon.equipo6.Repository;

import com.techU.hachathon.equipo6.models.Questions;

import java.util.List;

public interface QuestionsRepository {
    List<Questions> findAll();
    public Questions findOne(Integer ID);


}
