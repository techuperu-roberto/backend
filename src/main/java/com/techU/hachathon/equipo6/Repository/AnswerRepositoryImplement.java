package com.techU.hachathon.equipo6.Repository;

import com.techU.hachathon.equipo6.models.answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AnswerRepositoryImplement implements AnswerRepository{
    private final MongoOperations mongoOperations;

    @Autowired
    public AnswerRepositoryImplement (MongoOperations mongoOperations)  {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<answers> findAll(){
        List<answers> Id = this.mongoOperations.find(new Query(), answers.class);
        return Id;
    }

    @Override
    public answers findOne(String id) {
        answers encontrado = this.mongoOperations.findOne(new Query(Criteria.where("id").is(id)),answers.class);
        return encontrado;
    }

    @Override
    public answers saveAnswers(answers ans) {
        this.mongoOperations.save(ans);
        return findOne(ans.getId());

    }

    @Override
    public void updateAnswers(answers ans) {
        this.mongoOperations.save(ans);
    }

    @Override
    public void deleteAnswer(answers id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), answers.class);
    }

}
