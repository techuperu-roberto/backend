package com.techU.hachathon.equipo6.Repository;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.mongodb.Mongo;
import com.mongodb.client.MongoClient;
import com.techU.hachathon.equipo6.models.Questions;
import org.bson.conversions.Bson;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ConfigurationProperties(prefix = "mongodb")
public class QuestionsRepositoryImpl implements QuestionsRepository{
    private final MongoOperations mongoOperations;
    //private final MongoClient mongoClient;
    @Autowired
    public QuestionsRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Questions> findAll(){
        List<Questions> questions = this.mongoOperations.find(new Query(),Questions.class);
    //    List<Questions> questions = this.mongoClient.getDatabase("hackathon").getCollection("questions").find(new Query(),Questions.class);
        return questions;
    }

    @Override
    public Questions findOne(Integer ID){
        Questions encontrado = this.mongoOperations.findOne(new Query(Criteria.where("questionsID").is(ID)),Questions.class);
        //Gson json =new Gson();
        //Questions encontrado = json.fromJson(String.valueOf(this.mongoClient.getDatabase("hackathon").getCollection("questions").find(new Query(Criteria.where("questionsID").is(ID)))), Questions.class);
        return encontrado;
    }

}
